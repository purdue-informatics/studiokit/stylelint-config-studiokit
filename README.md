# stylelint-config-studiokit

> Shared config for stylint.

Extends [`stylelint-config-standard`](https://github.com/stylelint/stylelint-config-standard).

## Installation

```bash
yarn add --dev stylelint-config-studiokit stylelint@^13.0.0
```

## Usage

Set your `stylelint` config to:

```json
"stylelint": {
  "extends": "stylelint-config-studiokit"
}
```

## [License](LICENSE)
